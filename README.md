# image-resize

## What It Does
This package allows you to resize images stored in a directory based on given options for the command and save them in a custom folder.also you can run it as scheduled command.

## Usage
```php
// install laravel and packages
composer install

// run resize command just only one time
php artisan image:resize --source=[source directory path] --dist=[destination directory path] --width=[width of output images in pixel] --height=[height of output images in pixel] --count=[number of images to resize every time command runs]

// an example to resize images stored in /storage/app/images folder and save in public/resized-images folder.
// new size of images will be 100px * 100px and only 5 image will be resized
php artisan image:resize --source=/storage/app/images --dist=/public/resized-images --width=100 --height=100 --count=5

// an example to resize images stored in /storage/app/images folder and save in public/resized-images folder.
// new size of images will be 200px * auto and only 10 image will be resized
php artisan image:resize --source=/storage/app/images --dist=/public/resized-images --width=200 --count=10

// an example to resize images stored in /storage/app/images folder and save in public/resized-images folder.
// new size of images will be auto * 300px and only 10 image will be resized
php artisan image:resize --source=/storage/app/images --dist=/public/resized-images --height=300 --count=10

// To run as a scheduled command add line below to your operating system cron jobs list or run it directly in you console. 
php artisan schedule:work
// to customize schedule options check /app/Console/Kernel.php file.
```
