<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Intervention\Image\Facades\Image;

use File;

class ResizeImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image:resize {--source=} {--dist=} {--width=0} {--height=0} {--count=10}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'resize images base on given size and count';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $source = base_path($this->option('source'));
        $dist = base_path($this->option('dist'));

        $width = (int)$this->option('width');
        $width = $width > 0 ? $width : null;

        $height = (int)$this->option('height');
        $height = $height > 0 ? $height : null;

        $count = (int)$this->option('count');

        $resizeCallback = null;

        if(!file_exists($source) || !is_dir($source)){
            print "Input path not exists!\n";
            return 0;
        }

        if($count <= 0){
            print "Please enter a number greater than 0!\n";
            return 0;
        }

        if(!$width && !$height){
            print "You must enter at least one of the width or height options!\n";
            return 0;
        }
        
        if(!$width || !$height){
            $resizeCallback = function ($constraint) {
                $constraint->aspectRatio();
            };
        }

        if(!file_exists($dist)){
            mkdir($dist);
        }

        $files = File::files($source);

        $files_count = count($files);

        $count = $count >= $files_count ? $files_count : $count;

        for($i = 0; $i < $count; $i++){
            $img = Image::make($files[$i])->resize($width, $height, $resizeCallback);
            $img->save($dist . '/' . $files[$i]->getFileName());
        }

        return 0;
    }
}
